async function setCommandKey(optionKey, testName, testValue, defaultValue) {
  consoleDebug("[QuoteColors]: setCommandKey(): " + optionKey + ": " + testValue);

  let detail = {};
  detail.name = testName;
  detail.shortcut = testValue;
  if(detail.shortcut === "") {
    consoleDebug("[QuoteColors]: setCommandKey(): Your chosen command key is empty. Therefore the default \"" + defaultValue + "\" will be used.");
    detail.shortcut = defaultValue;
  }
  try {
    await browser.commands.update(detail);
  } catch (e) {
    consoleDebug("[QuoteColors]: setCommandKey(): Your chosen command key isn't valid. Therefore the default \"" + defaultValue + "\" will be used.");
    detail.shortcut = defaultValue;
    await browser.commands.update(detail);
    // Reset option to default
    return messenger.storage.local.remove(optionKey);
  }
}

async function initCommandKeys() {
  await reloadOption("commandKey");
  setCommandKey("commandKey", "toggle-all-quotes", options.commandKey, DefaultOptions.commandKey);

  await reloadOption("commandKeyCollapseOneLevel");
  setCommandKey("commandKeyCollapseOneLevel", "collapse-one-more-level", options.commandKeyCollapseOneLevel, DefaultOptions.commandKeyCollapseOneLevel);

  await reloadOption("commandKeyExpandOneLevel");
  setCommandKey("commandKeyExpandOneLevel", "expand-one-more-level", options.commandKeyExpandOneLevel, DefaultOptions.commandKeyExpandOneLevel);
}

initCommandKeys();