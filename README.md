***Quote Colors***

--------

### Version series

* Version 4.*   - Thunderbird 102.*
* Version 3.*   - Thunderbird 91.* - Migration to a MailExtension
* Version 2.*   - Thunderbird 78.*
* Version 1.*   - Thunderbird 68.*

### Features

* Configure text, border and background colors of quotes in mail and news messages
* Optionally configure main text, link and signature colors
* Optionally use the chosen colors in compose window, too (from up version 3.0a3)
* Provide separate colors for default (light) and dark mode by detecting "@ media (prefers-color-scheme: dark)" (from up version 3.0a5)
* Incorporate optionally collapsing quotes by click (from up version 3.0a6 / options dialogue from up Version 3.0a7)
* Shortcut Alt+Q to toggle between fully expanded and fully collapsed quotes, when collapse feature is enabled in options (from up 3.2.4). From up version 4.* the command key is configurable.

### Known issues

* It's not really an issue, but you should know, that Quote Colors doesn't provide all features in compose window. It is intended to have only a subset of features in compose window.
* MailExtension-API doesn't support the features to style printed messages. So Quote Colors doesn't work for printed messages, until the API is improved / added in Thunderbird itself.

### Installation

1. [Download Quote Colors from the official Thunderbird add-on page](https://addons.thunderbird.net/addon/quotecolors/)
2. [Installing an Add-on in Thunderbird](https://support.mozilla.org/kb/installing-addon-thunderbird)


### Contribution

You are welcomed to contribute to this project by:
* adding or improving the localizations of AttachmentExtractor Continued via email to me or a post in german Thunderbird forums [Thunderbird Mail DE](https://www.thunderbird-mail.de/forum/board/81-hilfe-und-fehlermeldungen-zu-thunders-add-ons/) or just create an [issue](https://gitlab.com/ThunderbirdMailDE/quote-colors/issues/) here on GitLab
* creating [issues](https://gitlab.com/ThunderbirdMailDE/quote-colors/issues/) about problems
* creating [issues](https://gitlab.com/ThunderbirdMailDE/quote-colors/issues/) about possible improvements


### Coders

* Alexander Ihrig (Maintainer, Update Thunderbird 68 + 78 + 91 + 102)
* Intika (Update Thunderbird 60)
* Malte Rücker (Original "Quote Colors" author)
* Qeole (Some parts of the new QuoteColors MailExtension code were taken from "Colored Diffs" (MPL 2.0 license) by Qeole and adapted for QuoteColors.)
* Michael J Gruber (Some parts of the new QuoteColors MailExtension code were taken from "QuoteCollapse" (MPL 1.1 license) by Michael J. Gruber.)
* Marxin (Feature additions for collapsing subquotes and by level)
* Stanimir Stamenkov (Idea and CSS for the option to show some more lines for the first collapsed quote level)

### Translators

* Tomas Brabenec (Czech)
* Jørgen Rasmussen (Danish)
* Alexander Ihrig (German)
* Carlos (Spanish [Spain])
* Olivier (French)
* Szalai Kálmán (Hungarian)
* Pietro Fistetto (Italian)
* Masahiko Imanaka (Japanese)
* Milupo (Lower Sorbian)
* Bartosz Piec (Polish)
* Igor Lyubimov (Russian)
* Milupo (Upper Sorbian)


### License

[Mozilla Public License version 2.0](https://gitlab.com/ThunderbirdMailDE/quote-colors/LICENSE)
