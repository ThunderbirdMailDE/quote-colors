var parentElements = [
  "enableQuoteCollapse",
  "quoteCollapseByDefault",
  "bordermode0",
  "bordermode1",
  "usermsgcolors",
];

// onLoad listener to load the i18n locale strings
document.addEventListener('DOMContentLoaded', () => {
  i18n.updateDocument();
}, { once: true });

// onLoad listener to build the UI and Preview
document.addEventListener('DOMContentLoaded', () => {
  consoleDebug("[QuoteColors] [options_ui_listeners.js]: DOMContentLoaded, restoreAllOptions()");
  restoreAllOptions()
  .then(() => { checkThinDouble(); })
  .then(() => { setDisabledAttribute(parentElements); })
  .then(() => { updatePreview(); });
});

// onChange listener for all options to save the changed options
OptionsList.forEach((option) => {
  consoleDebug("option: " + option);
  switch(option) {
    case "borderMode":
      document.getElementById("bordermode0").addEventListener("change", (e) => {
        consoleDebug("[QuoteColors] [options_ui_listeners.js]: Option borderMode-0 changed, saveOptions()");
        saveOptions(e)
        .then(() => { updatePreview(); });
      });
      document.getElementById("bordermode1").addEventListener("change", (e) => {
        consoleDebug("[QuoteColors] [options_ui_listeners.js]: Option borderMode-1 changed, saveOptions()");
        saveOptions(e)
        .then(() => { updatePreview(); });
      });
      break;

    case "commandKey":
      document.getElementById("commandKey").addEventListener("change", async (e) => {
        if(await testCommandKey("commandKey", DefaultOptions.commandKey, "toggle-all-quotes", document.getElementById("commandKey").value) == true) {
          consoleDebug("[QuoteColors] [options_ui_listeners.js]: Option commandKey changed, saveOptions()");
          showCommandKeyWarning("warning_optionCommandKey", false);
          saveOptions(e);
        } else {
          showCommandKeyWarning("warning_optionCommandKey", true);
        }
      });
      break;

    case "commandKeyCollapseOneLevel":
      document.getElementById("commandKeyCollapseOneLevel").addEventListener("change", async (e) => {
      if(await testCommandKey("commandKeyCollapseOneLevel", DefaultOptions.commandKeyCollapseOneLevel, "collapse-one-more-level", document.getElementById("commandKeyCollapseOneLevel").value) == true) {
        consoleDebug("[QuoteColors] [options_ui_listeners.js]: Option commandKeyCollapseOneLevel changed, saveOptions()");
        showCommandKeyWarning("warning_optionCommandKeyCollapseOneLevel", false);
        saveOptions(e);
      } else {
        showCommandKeyWarning("warning_optionCommandKeyCollapseOneLevel", true);
      }
      });
      break;

    case "commandKeyExpandOneLevel":
      document.getElementById("commandKeyExpandOneLevel").addEventListener("change", async (e) => {
      if(await testCommandKey("commandKeyExpandOneLevel", DefaultOptions.commandKeyExpandOneLevel, "expand-one-more-level", document.getElementById("commandKeyExpandOneLevel").value) == true) {
        consoleDebug("[QuoteColors] [options_ui_listeners.js]: Option commandKeyExpandOneLevel changed, saveOptions()");
        showCommandKeyWarning("warning_optionCommandKeyExpandOneLevel", false);
        saveOptions(e);
      } else {
        showCommandKeyWarning("warning_optionCommandKeyExpandOneLevel", true);
      }
      });
      break;

    default:
      document.getElementById(option).addEventListener("change", (e) => {
        consoleDebug("[QuoteColors] [options_ui_listeners.js]: Other Option changed, saveOptions()");
        saveOptions(e)
        .then(() => { updatePreview(); });
      });
  }
});

// reset click listener
document.getElementById("reset").addEventListener("click", () => {
  consoleDebug("[QuoteColors] [options_ui_listeners.js]: Reset clicked, resetAllOptions()");
  resetAllOptions()
  .then(() => { waitAndUpdatePreview(); });
});

// reset click light mode colors listener
document.getElementById("reset_colors").addEventListener("click", () => {
  consoleDebug("[QuoteColors] [options_ui_listeners.js]: Reset Light-Mode Colors clicked, resetLightmodeColorOptions()");
  resetLightmodeColorOptions()
  .then(() => { waitAndUpdatePreview(); });
});

// reset dark mode colors click listener
document.getElementById("dm_reset_colors").addEventListener("click", () => {
  consoleDebug("[QuoteColors] [options_ui_listeners.js]: Reset Dark-Mode Colors clicked, resetDarkmodeColorOptions()");
  resetDarkmodeColorOptions()
  .then(() => { waitAndUpdatePreview(); });
});

// check Thin versus Double borders listener
document.getElementById("borderWidth").addEventListener("change", (e) => {
  checkThinDouble();
});
document.getElementById("borderStyle").addEventListener("change", (e) => {
  checkThinDouble();
});

// onChange listeners to invoke setAttributeDisabled for nested options
parentElements.forEach((parentElement) => {
  consoleDebug("[QuoteColors] [options_ui_listeners.js] [parentElements.forEach] parentElement: " + parentElement);
  document.getElementById(parentElement).addEventListener("change", (e) => {
    setDisabledAttribute(parentElements);
  });
});
